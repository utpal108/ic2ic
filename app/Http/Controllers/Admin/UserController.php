<?php

namespace App\Http\Controllers\Admin;

use App\Mail\UserRegistration;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Validation\Rule;
use App\DataTables\UsersDataTable;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'phone_no' => 'required|unique:users',
            'registration_id' => 'required|unique:users',
            'status' => 'required',
            'password' => 'required',
            'speciality' => 'required',
            'designation' => 'required',
            'institution' => 'required',
            'user_type' => 'required',
            'address' => 'required',
        ]);

        $allData=$request->all();
        $allData['password']=bcrypt($request->password);

        if ($request->hasFile('profile_image')){
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $allData['profile_image']=$path;
        }
        $user=User::create($allData);

        $messageData['title']='User registration message';
        $messageData['registration_id']=$user->registration_id;
        Mail::to($user->email)->send(new UserRegistration($messageData));

        flash('User created successfully');
        return redirect()->action('Admin\UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        return view('admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            // Ignore user id
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'phone_no' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'registration_id' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'status' => 'required',
            'speciality' => 'required',
            'designation' => 'required',
            'institution' => 'required',
            'user_type' => 'required',
            'address' => 'required',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->phone_no=$request->phone_no;
        $user->status=$request->status;
        $user->registration_id=$request->registration_id;
        $user->phone_no=$request->phone_no;
        $user->speciality=$request->speciality;
        $user->designation=$request->designation;
        $user->institution=$request->institution;
        $user->user_type=$request->user_type;
        $user->address=$request->address;
        if (isset($request->password)){
            $user->password=bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')){
            Storage::delete($user->profile_image);
            $path=$request->file('profile_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(130, 130)->encode();
            Storage::put($path, $image);
            $user->profile_image=$path;
        }
        $user->save();
        flash('User updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        flash('User deleted successfully');
        return redirect()->action('Admin\UserController@index');
    }
}
