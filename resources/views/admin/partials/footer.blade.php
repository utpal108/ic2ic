<footer class="sa-page-footer">
    <div class="d-flex align-items-center w-100 h-100">
        <div class="footer-left">
            @copyright reserved by- <span class="footer-txt">IC2IC </span> &copy; 2018-{{ date('Y') }}
        </div>
    </div>
</footer>
