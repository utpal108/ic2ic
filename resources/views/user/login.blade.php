@extends('layout.app')

@section('page_title',' | Login')

@section('contents')
    <section id="reg-form" class="mt-0 pt-0">
        <div class="container">
            <div class="registration-modal mt-0" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body text-center my-auto">
                            <img src="/images/ic2ic-reg-logo.png" alt="">
                            <img src="/images/ic2ic-reg-full-form.png" alt="">
                            <h5>Check your registered Information</h5>
                            <form action="{{ action('UserController@user_authenticate') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="credential" class="form-control" id="regEmailIdOrPone" aria-describedby="emailHelp" placeholder="Enter Email Id Or Phone Number" required>
                                </div>
                                <button type="submit" class="ic-reg-btn-modal">Check Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
