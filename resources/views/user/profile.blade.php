@extends('layout.app')

@section('page_title',' | Profile')

@section('contents')
    <section id="reg-form">
        <div class="container">
            <div class="ic-full-content">
                <p class="ic-reg-box">Registration Form</p>
                <table>
                    <tr>
                        <td>Full Name</td>
                        <td>:</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>Specialty Name</td>
                        <td>:</td>
                        <td>{{ $user->speciality }}</td>
                    </tr>
                    <tr>
                        <td>Designation</td>
                        <td>:</td>
                        <td>{{ $user->designation }}</td>
                    </tr>
                    <tr>
                        <td>Institution</td>
                        <td>:</td>
                        <td>{{ $user->institution }}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ $user->address }}</td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td>:</td>
                        <td>{{ $user->phone_no }}</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>:</td>
                        <td>{{ $user->email }}</td>
                    </tr>

                    <tr>
                        <td>Registration ID</td>
                        <td>:</td>
                        <td>{{ $user->registration_id }}</td>
                    </tr>
                </table>
                <a href="/" class="ic-reg-home-link"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Back To Home</a>
            </div>
        </div>
    </section>
@endsection
