@component('mail::message')
# Congratulations!!

You have been registered for IC2IC( Ibrahim Cardiac Conference on Interventional Cardiology).<br>
Your Registration ID: {{ $user_info['registration_id'] }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
