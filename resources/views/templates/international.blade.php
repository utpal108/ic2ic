@extends('layout.app')

@section('page_title',' | ' .$page->page_title)

@section('contents')
    <section id="ic-faculty-national" class="ic-international">
        <div class="container">
            <p class="ic-faculty-list">Faculty List</p>
            <table class="table">
                <caption>International Faculties</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Sl No</th>
                    <th scope="col">Member’s Name</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>01</td>
                    <td>Dr Ajit Mullasari (India)</td>
                </tr>
                <tr>
                    <td>02</td>
                    <td>Dr Stanley Amarasekara (Sri Lanka)
                    </td>
                </tr>
                <tr>
                    <td>03</td>
                    <td>Dr Hiroki Watanabe (Japan)
                    </td>
                </tr>
                <tr>
                    <td>04</td>
                    <td>Dr Hiroki Emori (Japan)</td>
                </tr>
                <tr>
                    <td>05</td>
                    <td>Dr Nimit Shah (India)</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
